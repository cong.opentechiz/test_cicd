package com.cong.cicd.controller;


import com.cong.cicd.model.User;
import com.cong.cicd.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/cicd")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/save")
    public User save(){
                User user = new User( "cong", "HN");
                userService.save(user);
                return user;
    }

    @GetMapping("/all")
    public ResponseEntity<List<User>> getAll(){
        List<User> userList = userService.getAllUsers();
        return new ResponseEntity<>(userList, HttpStatus.OK);
    }
}
